export default function () {
  this.namespace = '/api';

  this.get('/products', function () {
    return {
      "products": [
        {
          "id": "GR1",
          "name": "Green tea",
          "price": 3.11
        },
        {
          "id": "SR1",
          "name": "Strawberries",
          "price": 5
        },
        {
          "id": "CF1",
          "name": "Coffee",
          "price": 1.23
        }
      ]
    }
  });
}

