import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  basket: service(),
  quantity: 1,
  actions: {
    addItem(item) {
      this.basket.addItem(parseInt(this.quantity), item);
    }
  }
});
