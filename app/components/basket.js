import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  basket: service('basket'),
  actions: {
    remove(item) {
      this.basket.remove(item);
    }
  }
});
