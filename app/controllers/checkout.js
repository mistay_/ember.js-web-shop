import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  basket: service('basket'),
  actions: {
    removeItem(item) {
      this.basket.removeItem(item);
    }
  }
});
