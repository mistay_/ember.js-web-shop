import Service from '@ember/service';
import { computed, set } from '@ember/object';

export default Service.extend({
  items: null,
  init() {
    this._super(...arguments);
    this.set('items', []);
  },
  addItem(qty, item) {
    if (this.items.includes(item)) {
      const index = this.items.indexOf(item);
      const quantity = this.items[index].quantity + qty;
      set(this.items[index], "quantity", quantity);
      this.notifyPropertyChange('items');
    } else {
      item.quantity = qty;
      this.items.addObject(item);
    }
  },
  removeItem(item) {
    this.items.removeObject(item);
  },
  findItem(id) {
    const index = this.ids.indexOf(id);
    return this.items[index];
  },
  quantities: computed.mapBy('items', 'quantity'),
  itemsCount: computed.sum('quantities'),
  prices: computed.map('items', function (item) {
    return item.price * item.quantity;
  }),
  total: computed.sum('prices'),
  ids: computed.mapBy('items', 'id'),
  discount(item) {
      const noDiscount = {name: '', value: 0};
      if(item.id === 'GR1') {
        return item.quantity >= 2 ? {name: 'buy-one-get-one-free', value: ((Math.floor(item.quantity / 2) * item.price))} : noDiscount;
      }else if(item.id === 'SR1') {
        return item.quantity >= 3 ? {name: 'more than 3 packages of strawberries', value: (item.quantity * (item.price - 3.5))} : noDiscount;
      }else if(item.id === 'CF1') {
        return item.quantity >= 3 ? {name: 'more than 3 coffies', value: (item.quantity * (item.price / 3)) } : noDiscount;
      }else {
        return noDiscount;
      }
  },
  discounts: computed.map('items', function(item) {
      return this.discount(item);
  }),
  discountPrices: computed.mapBy('discounts', 'value'),
  totalDiscount: computed.sum('discountPrices'),
  totalPrice: computed('totalDiscount', function () { 
    return parseFloat(this.total - this.totalDiscount).toFixed(2);
  })
});